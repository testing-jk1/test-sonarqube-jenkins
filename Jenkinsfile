pipeline {
    agent any

    environment {
        // Variables de entorno para SonarQube
        SCANNER_HOME = tool 'SonarQubeScanner'
        SONARQUBE_URL = 'http://localhost:9000'
        SONARQUBE_CREDENTIALS = credentials('sonarqube-token')
    }

    stages {
        stage('Checkout') {
            steps {
                // Clonar el repositorio
                git url: 'https://github.com/tu_usuario/tu_repositorio.git', branch: 'main'
            }
        }

        stage('Build') {
            steps {
                // Compilar el proyecto con Maven
                sh 'mvn clean package'
            }
        }

        stage('SonarQube Analysis') {
            steps {
                script {
                    // Ejecutar el análisis de SonarQube
                    withSonarQubeEnv('SonarQube') {
                        sh "${SCANNER_HOME}/bin/sonar-scanner \
                            -Dsonar.projectKey=tu_proyecto \
                            -Dsonar.sources=. \
                            -Dsonar.host.url=${SONARQUBE_URL} \
                            -Dsonar.login=${SONARQUBE_CREDENTIALS}"
                    }
                }
            }
        }

        stage('Quality Gate') {
            steps {
                script {
                    // Esperar y verificar el resultado del Quality Gate
                    timeout(time: 5, unit: 'MINUTES') {
                        def qualityGate = waitForQualityGate()
                        if (qualityGate.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qualityGate.status}"
                        }
                    }
                }
            }
        }
    }

    post {
        always {
            // Publicar resultados de SonarQube
            echo 'SonarQube analysis complete.'
        }

        failure {
            // Acciones a realizar en caso de fallo del pipeline!!!!
            mail to: 'tu_correo@example.com',
                 subject: "Pipeline failed: ${currentBuild.fullDisplayName}",
                 body: "Something went wrong in the pipeline: ${currentBuild.fullDisplayName} (${env.BUILD_URL})"
        }
    }
}

